  #define encoder0PinA  2 //encodeur 0
#define encoder0PinB  8

#define encoder1PinA  3 // encodeur 1
#define encoder1PinB  9

#define encoder3PinA  1 // encoder 3
#define encoder3PinB  4

#define pin_rpm_motor_interrupt 7 // optical detector interruption pin
#define stroboblanc 10 // output pin strobo
#include "LedControl.h"
#define display_dln 11 // DIN MAX72
#define display_clk 12 // clock MAX72
#define display_loadcs 13 // loadcs MAX72
#define pin_onoff 6

int t_TTL = 100; // duration of strobo TTL pulse

int pulse_length[] = {50,100,200,300,400,500,600,700,800 }; // list of possible strobo pulses


float period_multiplication[] = {1,2,3,4,5,6};  // table of frequency multiplication
float period_shift[] = {0,0.001,0.002,0.003,0.004,0.005,0.006,0.007,0.008,0.009,0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1}; // idee du debut, changée


int rpmcount = 0; // count for motor trigger
int phase_count = 0; // count for the motor phase

unsigned long timeold = 0; // temporary time variable for motor speed calculation
unsigned long t_strobo_on = 0; // temporary time variable for strobo flash duration

unsigned long t_disp_rpm = 0; // time for the display of motor speed
unsigned long temps_moteur = 0; // time when the image is vertical
unsigned long t_flash = 0; // time for the flasj trigger
unsigned long tinit = 0;

float T_moteur = 50000; // motor period in microsecond
unsigned long permanent_flash_shift = 0; // permanent phase shift in microsecond

int flash_cnt = 0; // count the flash number before the new t0 of motor
int shift_cnt = 0;// count for the shifted flashs
float n = 1; // integer part of the frequency ratior
float m = 0;  // shift
int signm = 0; // integer, 0 if m is negative, 1 if m is positive
int smooth_divider = 3; // smoothing for rotary encoder
int encoder0Pos = 0; //count of clicks on encoder 0
int encoder1Pos = 0;//count of clicks on encoder 1
int encoder3Pos = 0;//count of clicks on encoder 3

float n1 = n;
float n2 = 1;

int ticksCodeur_frq = 0; // integer for frequency table reading
int ticksCodeur_shift = 0; // integer for shift table reading
int ticksCodeur_pulse = 0; // integer for pulse duration table reading
int ticks_frq_max = 0; // max value of the integer of freq table
int ticks_pulse_max = 0; // max value of the integer of pulse length table
int ticks_shift_max = 20; // max value of the integer of shift table
int idx = 1;

boolean strobo = false; // bolean to do the flashing sequence
boolean start = false; // bolean to start flashes
boolean change = false; // boolean indicating a change of condition given by encoders

LedControl lc = LedControl(display_dln , display_clk , display_loadcs , 1);  // instance of the LED digits

void setup() { 
 
 Serial.begin(9600); // serial comm for debugging
  pinMode(stroboblanc,OUTPUT); // stroboscope triggering
  pinMode(pin_onoff,INPUT); // on off stroscope switch
  attachInterrupt(digitalPinToInterrupt(pin_rpm_motor_interrupt),motor_rpm,FALLING); //interrupt f the optical detector detecting the turn 
  attachInterrupt(digitalPinToInterrupt(encoder0PinA), doEncoder0, FALLING);  // encoder 0 interrupt on 1 pin
  attachInterrupt(digitalPinToInterrupt(encoder1PinA), doEncoder1, FALLING);  // encoder 1 interrupt 
  attachInterrupt(digitalPinToInterrupt(encoder3PinA), doEncoder3, FALLING);  // encoder 3 interrupt
  ticks_frq_max = sizeof(period_multiplication)/sizeof(period_multiplication[0]); // max encoder 0 value, depends on the multiplicative values disponible
  ticks_shift_max = sizeof(period_shift)/sizeof(period_shift[0]); // max encoder 1 value, depends on the shift values disponible
  ticks_pulse_max = sizeof(pulse_length)/sizeof(pulse_length[0]); // max encoder 3 value for selecting pulse length
  lc.shutdown(0,false); //wake up the MAX72XX from power-saving mode 
  lc.setIntensity(0,8); //set a medium brightness for the Leds
  printnum(n+m);
  sign(m);
}


void loop() { 
  
  
  if (millis()-t_disp_rpm > 1000)  {  // every second, check for on-off, calculate motor speed, and print erial debug
     vitesse_moteur();// calculate motor speed
     permanent_flash_shift = round(T_moteur*15/360); // set a shift for setting vertical image properly
     t_disp_rpm = millis(); // set the time variable variable to now
     if (digitalRead(pin_onoff) == HIGH) // if the on-off switch is on, then start is true
     {start = true;}
     else{start = false;}
//     Serial.print("m = ");
//     Serial.print(m);
      Serial.print("T moteur ");
     Serial.println(T_moteur);
     Serial.print("(flash_cnt+1)*(round((T_moteur/n))) + shift_cnt*round(T_moteur*m) - t_TTL ");
     Serial.println((flash_cnt+1)*(round((T_moteur/n))) + shift_cnt*round(T_moteur*m) - t_TTL);
     Serial.print(" m ");
     Serial.println( m );
     Serial.print(" n ");
     Serial.println(  n  );
     Serial.print(" flash_cnt ");
     Serial.println( flash_cnt );
     Serial.print(" shift_cnt ");
     Serial.println(  shift_cnt  );
  }
  
  // here to take into account the change given by the command pad; i.e. the encoders
  
 if(flash_cnt == 0 && change){ // take into account the change when the motor is at the vertical
    shift_cnt = 0; //set the shift to 0, to start teh sequence again
    n = period_multiplication[ticksCodeur_frq];  // read the table from the encoder
    if (ticksCodeur_shift < 0)// if the value is negative
    {m = -period_shift[-ticksCodeur_shift]; // set m to a negative value
  } 
    else{m = period_shift[ticksCodeur_shift];}//if the value positive
    sign(m); // set signm value
    t_TTL = pulse_length[ticksCodeur_pulse]; // set flash duration
    change = false; // bolean to say we did change the values
    printnum(n+m); // print the new value on LED digits
  } 
  
  // do the flashing sequence with the desired parameters
 stroboscope(n,m); /
}

void motor_rpm(){ // function assiciated to interrupt of the optical detector

  rpmcount += 1; // increase the count to calculate the period
  temps_moteur = micros(); // set teh time to now
  if (flash_cnt >= n){  flash_cnt = 0;} // if the number of flash per count is equal or superior to n, start again
}

void vitesse_moteur(){ // function to calculate the motor speed (the T_moteur, which is the time per round)
 // detachInterrupt(digitalPinToInterrupt(pin_rpm_motor_interrupt)); 
  T_moteur = round(( micros()-timeold )/rpmcount); // round motor period
  timeold = micros();  //actualize the time variable
  //  Serial.print("comptes = ");
  //  Serial.println(rpmcount);
  rpmcount = 0;  //set the turn count to zero
 // attachInterrupt(digitalPinToInterrupt(pin_rpm_motor_interrupt),motor_rpm,FALLING);
}

void stroboscope(int n, float m){ // declenchement du strobo avec la bonne periode
detachInterrupt(digitalPinToInterrupt(pin_rpm_motor_interrupt));  // remove interrupt, to avoid interference

// all the trick is here:
    if (micros()-temps_moteur > (flash_cnt+signm)*(round((T_moteur/n))) + shift_cnt*round(T_moteur*m) - signm*t_TTL + (1-signm)*permanent_flash_shift  && !strobo && start) 
 
    {flash_cnt += 1; // we did one flash --> increase the count
     if ( m != 0 ){shift_cnt += 1;} // and increase shift counrt of there is a shift
      attachInterrupt(digitalPinToInterrupt(pin_rpm_motor_interrupt),motor_rpm,FALLING); // set back the interrupt
      digitalWrite(stroboblanc,HIGH); // set the LED on for flash
      t_strobo_on = micros(); // time for flash duration
      strobo = true; // the flash is on
    }
    else{attachInterrupt(digitalPinToInterrupt(pin_rpm_motor_interrupt),motor_rpm,FALLING);} // set back the interrupt
  
  if (micros()-t_strobo_on > t_TTL && strobo) // to turn the flash off after its duration
      {
      digitalWrite(stroboblanc,LOW); // turn the LED off
      strobo = false; // the flash is off
      }
  if(shift_cnt == 0 &&  m!=0 ){tinit = micros();} // set the Tinit var
 
   if(shift_cnt >= round(abs(1/(m))/n) || micros()-tinit >= T_moteur*(abs(1/(m*n))*(2))+ permanent_flash_shift )  // to set the shift in phase
      {shift_cnt=0;} 
  
   if (micros()-t_strobo_on >= 2*T_moteur/n){ // if nothing happens during two motor turn, then reset
   flash_cnt = 0;
   shift_cnt = 0;
   }
}


void doEncoder0() { // encoder 0 interrupt function
  if (digitalRead(encoder0PinB) == HIGH) { // increase
    encoder0Pos = encoder0Pos + 1;
  } 
  if (digitalRead(encoder0PinB) == LOW) { // decrease
    encoder0Pos = encoder0Pos - 1;
  } 
   ticksCodeur_frq = encoder0Pos/smooth_divider; // smoothing the change
  if (ticksCodeur_frq > ticks_frq_max-1) //set the max
  {ticksCodeur_frq = ticks_frq_max-1;}
  if (ticksCodeur_frq  < 0) // set the min
  {ticksCodeur_frq = 0;}
  change = true;
}
  
void doEncoder1() { // same as doEncoder0, but for encoder1
  if (digitalRead(encoder1PinB) == HIGH) {
    encoder1Pos = encoder1Pos + 1;
  } 
  if (digitalRead(encoder1PinB) == LOW) {
    encoder1Pos = encoder1Pos - 1;
  }
  
  ticksCodeur_shift = encoder1Pos;
  if (ticksCodeur_shift > ticks_shift_max)
  {ticksCodeur_shift = ticks_shift_max;}
  if (ticksCodeur_shift  < -ticks_shift_max)
  {ticksCodeur_shift = -ticks_shift_max;}
  change = true;
} 

void doEncoder3() { // same as doEncoder0, but for encoder3
  if (digitalRead(encoder3PinB) == HIGH) {
    encoder3Pos = encoder3Pos + 1;
  } 
  if (digitalRead(encoder3PinB) == LOW) {
    encoder3Pos = encoder3Pos - 1;
  }
  
  ticksCodeur_pulse = encoder3Pos;
  if (ticksCodeur_pulse >= ticks_pulse_max)
  {ticksCodeur_pulse = ticks_pulse_max;}
  if (ticksCodeur_pulse  <= 0)
  {ticksCodeur_pulse = 0;}
  change = true;
} 

void printnum(float num) {// function to print the number on the LED digit display

    int v = num*1000;    
    int ones;
    int tens;
    int hundreds;
    int thousands;

    if( v > 9999) 
       return;

    // decompose the number in Thousands, hundreds; tens and unity
    ones=v%10;
    v=v/10;
    tens=v%10;
    v=v/10;
    hundreds=v%10;
    v=v/10;
    thousands=v;     

    //Now print the number digit by digit
    lc.setDigit(0,0,(byte)thousands,true);
    lc.setDigit(0,1,(byte)hundreds,false);
    lc.setDigit(0,2,(byte)tens,false);
    lc.setDigit(0,3,(byte)ones,false);
}

void sign(float number) // funciton to give the sign of m
{
  if (number < 0 ){signm = 1;}
  else{signm = 0;}

}


